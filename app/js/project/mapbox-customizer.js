mapboxgl.accessToken = 'pk.eyJ1IjoiYW5kcmJyYXQiLCJhIjoiY2tvOGpsMnhzMm4yaDJ3bXZ4azN4a2o0ZSJ9.UoBv7Y6c0dpb6uBQA5i_gw';
var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/andrbrat/cko8y4eok2hs717qu7rpgk60n',
  center: [37.600, 55.745],
  zoom: 15,
});

var geojson = {
  'type': 'FeatureCollection',
  'features': [{
      'type': 'Feature',
      'properties': {
        'message': 'Foo',
        'iconSize': [60, 60]
      },
      'geometry': {
        'type': 'Point',
        'coordinates': [37.600, 55.745]
      }
    }
  ]
};

geojson.features.forEach(function (marker) {
  // create a DOM element for the marker
  var el = document.createElement('div');
  el.className = 'marker';
  el.style.backgroundImage =
  'url(https://friendlycoder.ru/temp/win-black-marker.png)';
  el.style.width = marker.properties.iconSize[0] + 'px';
  el.style.height = marker.properties.iconSize[1] + 'px';
  el.style.backgroundSize = '100%';

  el.addEventListener('click', function () {
  window.alert(marker.properties.message);
  });

  // add marker to map
  new mapboxgl.Marker(el)
  .setLngLat(marker.geometry.coordinates)
  .addTo(map);
});