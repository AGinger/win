document.addEventListener('DOMContentLoaded', function() {

  let button = document.querySelectorAll('.tab-head-btn'),
      tab = document.querySelectorAll('.tab-body-item'),
      i;

  button.forEach( (btn) => {
    btn.addEventListener('click', () => {

      let tabId = btn.getAttribute('data-tab');

      for( i = 0; i < button.length; i++ ) {
        button[i].classList.remove('active');
      }

      for( i = 0; i < tab.length; i++ ) {
        tab[i].style.display = 'none';
      }

      btn.classList.add('active');
      document.getElementById('tab' + tabId).style.display = 'block';

    });
  });

});