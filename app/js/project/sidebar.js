(function(){
  let navIconOpen = document.getElementById('nav-icon-open'),
      navIconClose = document.getElementById('nav-icon-close'),
      sidebar = document.getElementById('sidebar');
  navIconOpen.addEventListener('click', function () {
    sidebar.classList.add('active');
  });
  navIconClose.addEventListener('click', function () {
    sidebar.classList.remove('active');
  });
})();