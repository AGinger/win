(function(){
  const slider = document.querySelector('.swiper-container');
  if (slider) {
    const swiper = new Swiper('.swiper-container', {
      autoplay: {
        delay: 3000,
      },
      speed: 1000,
      loop: true,
      effect: 'fade',
    });
  }
})();