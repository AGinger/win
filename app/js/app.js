require('./libs/modernizr-custom.js'); // adds lib to detect browser webp support
// Dependancies
require('./project/swiper-customizer.js'); // Swiper Slider Settings
require('./project/webp-customizer.js'); // set fallback for webp images
require('./project/sidebar.js'); // sidebar controls
require('./project/tabs.js'); // tabs
require('./project/input-file.js'); // form file button customization